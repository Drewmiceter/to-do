package com.example.student.guillermodoingwork;

public class GuillermosPost {

    public String title;
    public String dateAdd;
    public String dateDue;
    public String category;

    public GuillermosPost(String title, String dateAdd, String dateDue, String category) {
        this.title = title;
        this.dateAdd = dateAdd;
        this.dateDue = dateDue;
        this.category = category;
    }
}
