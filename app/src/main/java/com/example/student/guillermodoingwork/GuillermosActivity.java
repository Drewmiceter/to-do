package com.example.student.guillermodoingwork;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

public class GuillermosActivity extends SingleFragmentActivity implements ActivityCallback {
    public ArrayList<GuillermosPost> guillermosPosts = new ArrayList<GuillermosPost>();

    @Override
    protected Fragment createFragment() {
        return new GuillermosListFragment();
    }

    @LayoutRes
    @Override
    protected int getsLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {

    }
}