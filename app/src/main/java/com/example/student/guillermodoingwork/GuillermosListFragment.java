package com.example.student.guillermodoingwork;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class GuillermosListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    public GuillermosActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (GuillermosActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_guillermos_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        GuillermosPost i1 = new GuillermosPost("Guillermo", "10/1/12", "11/2/21", "Football");
        GuillermosPost i2 = new GuillermosPost("Dimitri", "10/2/13", "11/3/22", "Guitars");
        GuillermosPost i3 = new GuillermosPost("Sebastian", "10/3/14", "11/4/23", "Water Polo");
        GuillermosPost i4 = new GuillermosPost("Axel", "10/4/15", "11/5/24", "Baseball");

        activity.guillermosPosts.add(i1);
        activity.guillermosPosts.add(i2);
        activity.guillermosPosts.add(i3);
        activity.guillermosPosts.add(i4);



        updateUserInterface();

        return view;
    }

    public void updateUserInterface() {
        GuillermosPostAdapter adapter = new GuillermosPostAdapter(activityCallback, activity.guillermosPosts);
        recyclerView.setAdapter(adapter);
    }
}