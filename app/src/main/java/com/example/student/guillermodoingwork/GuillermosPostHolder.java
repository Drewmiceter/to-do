package com.example.student.guillermodoingwork;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class GuillermosPostHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public GuillermosPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView) itemView;
    }
}

