package com.example.student.guillermodoingwork;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
