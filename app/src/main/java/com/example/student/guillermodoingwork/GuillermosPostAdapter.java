package com.example.student.guillermodoingwork;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class GuillermosPostAdapter extends RecyclerView.Adapter<GuillermosPostHolder> {

    private ArrayList<GuillermosPost> guillermosPosts;
    private ActivityCallback activityCallback;

    public GuillermosPostAdapter(ActivityCallback activityCallback, ArrayList<GuillermosPost> guillermosPosts) {
        this.activityCallback = activityCallback;
        this.guillermosPosts = guillermosPosts;
    }

    @Override
    public GuillermosPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new GuillermosPostHolder(view);
    }

    @Override
    public void onBindViewHolder(GuillermosPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(guillermosPosts.get(position).title);
    }

    @Override
    public int getItemCount() {
        return guillermosPosts.size();
    }
}
